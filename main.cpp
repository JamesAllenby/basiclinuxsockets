#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(int argc, char* argv[])
{
    std::cout << "Starting linux sockets..." << std::endl;

    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_fd < 0)
    {
        perror("socket");
        std::exit(EXIT_FAILURE);
    }

    sockaddr_in server_address, client_address;

    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(10000);
    server_address.sin_addr.s_addr = INADDR_ANY;

    sockaddr* server_bind = reinterpret_cast<sockaddr*>(&server_address);
    if (bind(sock_fd, server_bind, sizeof(server_address)) < 0)
    {
        perror("bind");
        std::exit(EXIT_FAILURE);
    }

    if (listen(sock_fd, SOMAXCONN) < 0)
    {
        perror("listen");
        std::exit(EXIT_FAILURE);
    }

    socklen_t client_length = 0;


        int connfd = accept(sock_fd, (sockaddr*)&client_address, &client_length);
        if (connfd < 0)
            perror("accept");

        write(connfd, "ADA BOYS!!\n\n", 12);
        shutdown(connfd, SHUT_RDWR);

    close(sock_fd);



    return 0;
}
